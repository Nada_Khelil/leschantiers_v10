# encoding: utf-8
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from datetime import datetime, timedelta
import time

class account_provision_inv(osv.osv_memory):
    _name = "invoice.situation"
    _description = "Advance invoice"    
       
    _columns = {  
        'advance_payment_method':fields.selection(
            [('all', 'Final invoice'), ('situation', 'Situation')],
            'What do you want to invoice?', required=True,),
        'proj1':fields.many2one('project.project','situation'),
        'phase_line': fields.one2many('project.phase', 'id', 'Phases'),
        'final_inv':fields.integer('Final invoice'),
        'acompte_inv':fields.boolean('Acompte invoice')
                    }
    _defaults = {'advance_payment_method':'all',
                 'acompte_inv':False,
                 
                 }
    def onchange_method(self, cr, uid, ids, advance_payment_method,proj1, context=None): 
        if advance_payment_method == 'situation':
            res = {}
            if proj1:
                res['value'] = {'phase_line': []}
                phase_line = self.pool['project.phase'].search(cr, uid, [('project_id', '=', proj1), ('is_title', '=', False)], context=context)
                return {'value': {'phase_line' : phase_line}}

    
    def create_situation_inv(self, cr, uid, ids, context=None):
        tax_amount = rplp = 0.0
        account_ids = context.get('active_ids', [])
        project = self.pool.get('project.project').browse(cr, uid, account_ids[0], context=context)
        if not project.analytic_account_id.partner_id:
            raise osv.except_osv(_('Exception!'),
                                _('The contract don t include client'))
        else:
            product_ids = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'leschantiers', 'product_product_situation')
            if product_ids:
                product_id = product_ids[1]
            else:
                product_id = False
            if product_id:
                product = self.pool.get('product.product').browse(cr, uid, product_id)
            general_account = product.property_account_income or product.categ_id.property_account_income_categ
            if not general_account:
                raise osv.except_osv(_("Configuration Error!"), _("Please define income account for product '%s'.") % product.name)   
            taxes = product.taxes_id or general_account.tax_ids
            tax = self.pool.get('account.fiscal.position').map_tax(cr, uid, project.analytic_account_id.partner_id.property_account_position or False, taxes)
            offre_line = self.pool.get('sale.order').search(cr, uid, [('project_id', '=', project.analytic_account_id.id)], context=context)
            ids_discount = []
            for offre_id in offre_line :               
                offre = self.pool.get('sale.order').browse (cr, uid, offre_id)
                for discount in offre.discount_ids:
                    if not discount.arret:
                        ids_discount.append(discount.id) 
                tax_amount += offre.amount_tax_sale
                rplp += offre.rplp
            inv_values = {
                        'name': 'Situation invoice' + time.strftime('%d/%m/%Y') + " " + project.analytic_account_id.name,
                        'origin': project.analytic_account_id.name,
                        'partner_id': project.analytic_account_id.partner_id.id or False,
                        'company_id': project.analytic_account_id.company_id.id or False,
                        'payment_term': project.analytic_account_id.partner_id.property_payment_term.id or False,
                        'account_id': project.analytic_account_id.partner_id.property_account_receivable.id or project.analytic_account_id.partner_id.parent_id.property_account_receivable.id or False,
                        'currency_id': project.analytic_account_id.currency_id.id or 1,
                        'fiscal_position': project.analytic_account_id.partner_id.property_account_position.id or False,
                        'acompted': False,
                        'type':'out_invoice',
                        'invoice_type':'situation',
                        'taxes':tax_amount,
                        'amount_rplp':rplp,
                        'analytic_id': project.analytic_account_id.id or False,
                        'discount_ids': [(6, 0, ids_discount)]
                        }
            inv_id = self.pool.get('account.invoice').create(cr, uid, inv_values, context=context)  
            invoice_line_obj = self.pool.get('account.invoice.line')                   
            for phase in project.phase_line:
                if phase.type_phase == 'avenant':
                    name = 'Avenant : ' + phase.name
                else :
                    name = phase.name
                values = {
                          'name': name,
                          'libelle': phase.libelle,
                          'ref':phase.ref,
                          'quantity': 1.0,
                          'invoice_id': inv_id,
                          'product_id': product_id, 
                          'phase_amount':phase.sale_amount,
                          'advance':phase.advance_wiz,
                          'price_unit':phase.total_advance,
                          'uos_id': 1 or False,
                          'invoice_line_tax_id': [(6, 0, tax)],
                          'is_title':phase.is_title,
                          'is_advance':True,
                          'sequence':phase.sequence,
                          }
                invoice_line_obj.create(cr, uid, values, context=context) 
            self.pool.get('account.invoice').button_reset_taxes(cr, uid, [inv_id], context)                                   
            mod_obj = self.pool.get('ir.model.data') 
            res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
            res_id = res and res[1] or False,
            if res_id:
                return {
                    'name': _('Situation invoice'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'account.invoice',
                    'res_id': inv_id,
                    'view_id': [res_id] or False,
                    'type': 'ir.actions.act_window',
                }
                
    def create_final_inv(self, cr, uid, ids, context=None):
        tax_amount = rplp = 0.0
        account_ids = context.get('active_ids', [])
        project = self.pool.get('project.project').browse(cr, uid, account_ids[0], context=context)
        if not project.analytic_account_id.partner_id:
            raise osv.except_osv(_('Exception!'),
                                _('The contract don t include client'))
        else: 
#             configuration de taxes
            product_ids = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'leschantiers', 'product_product_situation')            
            if product_ids:
                product_id = product_ids[1]
            else:
                product_id = False
            if product_id:
                product = self.pool.get('product.product').browse(cr, uid, product_id)
            general_account = product.property_account_income or product.categ_id.property_account_income_categ
            if not general_account:
                raise osv.except_osv(_("Configuration Error!"), _("Please define income account for product '%s'.") % product.name)   
            taxes = product.taxes_id or general_account.tax_ids
            tax = self.pool.get('account.fiscal.position').map_tax(cr, uid, project.analytic_account_id.partner_id.property_account_position or False, taxes)
            
            product_arret_ids = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'leschantiers', 'product_product_arrete')            
            if product_arret_ids:
                product_arret_id = product_arret_ids[1]
            else:
                product_arret_id = False
            if product_arret_id:
                product_arret = self.pool.get('product.product').browse(cr, uid, product_arret_id)
            general_account_arret = product_arret.property_account_income or product_arret.categ_id.property_account_income_categ
            if not general_account_arret:
                raise osv.except_osv(_("Configuration Error!"), _("Please define income account for product '%s'.") % product_arret.name)   
            taxes_arret = product_arret.taxes_id or general_account_arret.tax_ids
            tax_arret = self.pool.get('account.fiscal.position').map_tax(cr, uid, project.analytic_account_id.partner_id.property_account_position or False, taxes_arret)
#             Fin config Tax
            offre_line = self.pool.get('sale.order').search(cr, uid, [('project_id', '=', project.analytic_account_id.id)], context=context)
            ids_discount = []
            inv_values = {
                        'name': 'Final Request' + time.strftime('%d/%m/%Y') + " " + project.analytic_account_id.name,
                        'origin': project.analytic_account_id.name,
                        'partner_id': project.analytic_account_id.partner_id.id or False,
                        'company_id': project.analytic_account_id.company_id.id or False,
                        'payment_term': project.analytic_account_id.partner_id.property_payment_term.id or False,
                        'account_id': project.analytic_account_id.partner_id.property_account_receivable.id or project.analytic_account_id.partner_id.parent_id.property_account_receivable.id or False,
                        'currency_id': project.analytic_account_id.currency_id.id or 1,
                        'fiscal_position': project.analytic_account_id.partner_id.property_account_position.id or False,
                        'acompted': True,
                        'type':'out_invoice',
                        'invoice_type':'facture_finale',
                        'taxes':tax_amount,
                        'analytic_id': project.analytic_account_id.id or False,
                        'discount_ids': [(6, 0, ids_discount)]
                        }
            inv_id = self.pool.get('account.invoice').create(cr, uid, inv_values, context=context)                
            invoice_line_obj = self.pool.get('account.invoice.line')                              
            for phase in project.phase_line:
                if phase.type_phase == 'avenant':
                    name = 'Avenant : ' + phase.name
                else :
                    name = phase.name
                values = {
                          'name': name,
                          'sequence':phase.sequence,
                          'libelle': phase.libelle,
                          'ref':phase.ref,
                          'quantity': 1.0,
                          'invoice_id': inv_id,
                          'product_id': product_id, 
                          'phase_amount':phase.sale_amount,
                          'advance':100,
                          'is_title':phase.is_title,
                          'is_advance':True,
                          'uos_id': 1 or False,
                          'discount_b':False,
                          'invoice_line_tax_id': [(6, 0, tax)],
                          'price_unit':phase.sale_amount,
                          }
                invoice_line_obj.create(cr, uid, values, context=context)            
            
            for offre_id in offre_line :               
                offre = self.pool.get('sale.order').browse (cr, uid, offre_id)
                for discount in offre.discount_ids:
                    if not discount.arret:
                        ids_discount.append(discount.id)
                    else:
                        values = {
                          'name': "Arrété",
                          'quantity': 1,
                          'invoice_id': inv_id,
                          'product_id': product_arret_id,
                          'phase_amount':0,
                          'price_unit':-discount.discount_amount,
                          'advance':100,
                          'discount_b':True,
                          'is_title':False,
                          'is_advance':False,
                          'uos_id': 1 or False,
                          'invoice_line_tax_id': [(6, 0, tax_arret)],
                          }
                        invoice_line_obj.create(cr, uid, values, context=context)           
                    
                for line in offre.discount_line:
                    if not line.arrete_b:
                        values = {
                          'name': line.name,
                          'quantity': 1,
                          'invoice_id': inv_id,
                          'product_id': product_id,
                          'phase_amount':0,
                          'price_unit':line.price_unit,
                          'advance':100,
                          'discount_b':True,
                          'is_title':False,
                          'is_advance':False,
                          'uos_id': 1 or False,
                          'invoice_line_tax_id': [(6, 0, tax)] or False
                          }                  
                        invoice_line_obj.create(cr, uid, values, context=context)
                tax_amount += offre.amount_tax_sale
                rplp += offre.rplp
            self.pool.get('account.invoice').write(cr, uid, [inv_id], {'discount_ids': [(6, 0, ids_discount)], 'taxes':tax_amount, 'rplp':rplp})
            self.pool.get('account.invoice').button_reset_taxes(cr, uid, [inv_id], context)
            mod_obj = self.pool.get('ir.model.data') 
            res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
            res_id = res and res[1] or False,
            if res_id:
                return {
                    'name': _('Final invoice'),
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'account.invoice',
                    'res_id': inv_id,
                    'view_id': [res_id] or False,
                    'type': 'ir.actions.act_window',
                }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
     
