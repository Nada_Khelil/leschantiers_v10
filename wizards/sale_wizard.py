# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import odoo.exceptions
import odoo.osv.osv
import odoo.addons.decimal_precision as dp
import logging
from datetime import datetime
_logger = logging.getLogger(__name__)

#-------------------------------------------------------------------------------
# Offre_wizard
#-------------------------------------------------------------------------------
     
class sale_total_arrete(models.TransientModel):
    _name = "sale.arret"
    _description = "Arreter facturation a value"  
    
    total_arrete = fields.Float(string='Total Arrete', digits_compute=dp.get_precision('Product Price'))
     
     
    def calculer_total_arrete(self):
        """ This method recalcul the total amount a arreter in sale order""" 
        vals = {} 
        res = {}
        arrete = 0 
        sale_obj = self.env['sale.order']
        discount_obj = self.env['sale.discount.line']
        line_obj = self.env['sale.order.line']
        order = sale_obj.browse(self._context.get('active_ids'))[0]

        product_ids = self.env['ir.model.data'].get_object_reference('leschantiers', 'product_product_arrete')
        if product_ids:
            product_id = product_ids[1]
        else:
            product_id = False
        if product_id:
            product = self.env['product.product'].browse(product_id)
            general_account = product.property_account_income or product.categ_id.property_account_income_categ
        if not general_account:
            raise odoo.osv.except_osv(_("Configuration Error!"), _("Please define income account for product '%s'.") % product.name)   
        taxes = product.taxes_id or general_account.tax_ids
        tax = self.env['account.fiscal.position'].map_tax(order.partner_id.property_account_position or False, taxes)
        
        arrete = order.amount_total_TTC - self.total_arrete
        discount = discount_obj.search([('arret', '=', True), ('order_id', '=', order.id)])
        vals['arrete'] = arrete          
        if discount:
            discount.write({'price_unit': arrete,'discount_amount':arrete})
            discount.line_id.write({'price_unit': - arrete})
        else:
            data = {'name': 'Arrété' + ' ' + datetime.datetime.now().strftime("%d%m%y"),
                        'order_id': order.id,
                        'price_unit': - arrete, 
                        'product_id': product_id,
                        'tax_id': [(6, 0,tax)],
                        'state': 'draft', 
                        'discount_b': True, 
                        'arrete_b': True,
                                            } 
                 
            line_id = line_obj.create(data) 
            data_discount = {
                              'name': "Arrété",
                              'order_id': order.id,
                              'price_unit': arrete,
                              'line_id': line_id,
                              'discount_amount': arrete,
                              'discount_method' : 'fixed',
                              'discount_pr': 0,
                              'arret':True,
                              'discount_type':"Arreté",
                                            }                 
            discount_obj.create(data_discount) 
        order.write(vals) 
        return {'type': 'ir.actions.act_window_close'}
                        

class position_title(models.TransientModel): 
    _name = "position.title"
    _description = "Position Title"
    
    sequence = fields.Integer('Sequence',default=5)
    name = fields.Char('Titre', required=True)
    num = fields.Char('Numéro ')
    order_id = fields.Many2one('sale.order', 'Order Reference')
    
    def add_position_title(self):
        data = {}
        position_obj = self.env['chantier.position']
        for title in self:
            data = {
                  'name': title.name,
                  'num': title.num,
                  'order_id' : title.order_id.id,
                  'is_title': True,
                  'sequence':title.sequence,
                  'state':'draft'
                    }
        position_obj.create(data)
        return {'type': 'ir.actions.act_window_close', }


class sale_order_line_title(models.TransientModel): 
    _name = "sale.title"
    _description = "sale Order Line Title"
    
    
    sequence = fields.Integer(string='Sequence',default=5)
    name = fields.Char(string='Titre', required=True)
    order_id = fields.Many2one('sale.order', string='Order Reference')  
    
    def add_sale_order_title(self):
        data = {}
        position_obj = self.env['sale.order.line']
        for title in self:
            data = {
                  'name': title.name,
                  'order_id' : title.order_id.id,
                  'is_title': True,
                  'sequence':title.sequence,
                    }
            position_obj.create(data)
        return {'type': 'ir.actions.act_window_close', }

