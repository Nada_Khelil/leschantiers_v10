# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import odoo.exceptions
import odoo.osv.osv
import odoo.addons.decimal_precision as dp
import math
import logging
_logger = logging.getLogger(__name__)
#-------------------------------------------------------------------------------
# sale_order_line
#-------------------------------------------------------------------------------
class sale_order_line(models.Model):
    _inherit = "sale.order.line"  
    
    #-------------------------------------------------------------------------------
    # FIELDS
    #-------------------------------------------------------------------------------     
    order_id = fields.Many2one('sale.order', string='Order Reference', ondelete='cascade',copy=False)
    poids_volumique = fields.Float(string="Poids Volumique (dm3)", help="Poids pour 1m2 x 1mm d'épaisseur.")
    account_id = fields.Many2one('account.analytic.account', string='Chantier/Position')  # Id pour position modèle  
    posit_id = fields.Many2one('chantier.position', string='Positions', readonly=True, ondelete='cascade')  # Id pour les position modèles
    posit_tache_id = fields.Many2one('chantier.position', string='Positions', readonly=True, ondelete='cascade')
    price_revient = fields.Float(string='Prix de revient', digits=dp.get_precision('Product Price'), readonly=True, states={'draft': [('readonly', False)]})  # prix d'achat du position,
    from_contract = fields.Boolean(string='Discout from Contract Reference')
    qty_advance = fields.Float(string='Consumed Quantity', digits=dp.get_precision('Quantity Advancement'), readonly=True, states={'draft': [('readonly', False)]})  # quatité d'avacement de consommation
    progress_product_receiving = fields.Float(compute='_progress_get', store=True, readonly=True, string='Working Time Progress (%)')
    name = fields.Char(string='Description', required=True)
    qty = fields.Float(string='Quantity',default=1.0)
    longueur = fields.Float(string='Longueur',default=1.0)
    largeur = fields.Float(string='Largeur',default=1.0)
    diametre = fields.Float(string='Diamètre')
    frais_livraison_speciaux = fields.Float('Frais de Livraisons spéciaux')
    unite_mesure_frais_livraison = fields.Many2one("product.uom", string='Unité')
    frais_usinage = fields.Float(string='Frais d\'usinage')
    unite_mesure_frais_usinage = fields.Many2one("product.uom", string='Unité')
    marge = fields.Float(string='Marge article')
    type_vente = fields.Selection([('kg', 'Poids'), ('m2', 'Surface'), ('ml', 'ML'), ('pce', 'PCE'), ], 
                                  string="Calcul basé sur", default='pce',required=True)
    surface_unitaire = fields.Float(string="Surface unitaire", digits=(13, 4))
    surface_total = fields.Float(string="Surface totale", digits=(13, 4))
    th_weight_total = fields.Float(string="Surface totale", digits=(13, 4))
    prix_achat_unitaire = fields.Float(string='Prix unitaire d\'achat', digits=dp.get_precision('Product'))
    prix_vente_total = fields.Float(string='Prix Total d\'achat', digits=dp.get_precision('Product'))
    prix_vente_total_unitaire = fields.Float(string='Total prix par position', digits=dp.get_precision('Product'))
    frais_livraison_speciaux = fields.Float(string='Frais de livraisons spéciaux')
    frais_usinage_total = fields.Float(string='Frais d\'usinage en Frs')
    frais_usinage_unitaire = fields.Float(string='Frais d\'usinage en Frs')
    total_price = fields.Float(string='Prix Total', digits=dp.get_precision('Product'))
    product_id = fields.Many2one('product.product', string="Produit")
    product_uom = fields.Many2one('product.uom')
    delay_fabric = fields.Float(string='Delai de Fabrication', digits=dp.get_precision('Product'))
    is_title = fields.Boolean(string='Utiliser titre',default=False)
    price_subtotal = fields.Monetary(compute='_compute_amount', string='Subtotal', readonly=True, store=True)
    tax_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    #ADD
    create_task = fields.Boolean(string="create task",default=False)
    th_weight = fields.Float('Weight', readonly=True, states={'draft': [('readonly', False)]}, digits=dp.get_precision('Stock Weight'))
    product_uos_qty = fields.Float('Quantity (UoS)' ,digits = dp.get_precision('Product UoS'), readonly=True, states={'draft': [('readonly', False)]})
    assigned_to = fields.Many2one('res.users',string='Assigned to')
    product_uos = fields.Many2one('product.uom', 'Product UoS')
    address_allotment_id = fields.Many2one('res.partner', 'Allotment Partner',help="A partner to whom the particular product needs to be allotted.")
    
    #-------------------------------------------------------------------------------
    # COMPUTE METHODS
    #-------------------------------------------------------------------------------
    @api.depends('qty_advance', 'product_uom_qty')
    def _progress_get(self): 
        for line in self: 
            if line.qty_advance != 0:
                line.progress_product_receiving = round(100.0 * line.qty_advance / line.product_uom_qty) 
            else:
                line.progress_product_receiving = 0.0
               
    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id','total_price')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
            if line.total_price > 0:
                line.update({
                   'price_tax': taxes['total_included'] - taxes['total_excluded'],
                   'price_total': taxes['total_included'],
                   'price_subtotal': line.total_price
                })
            else:
                line.update({
                    'price_tax': taxes['total_included'] - taxes['total_excluded'],
                    'price_total': taxes['total_included'],
                    'price_subtotal': taxes['total_excluded'],
                })
    #-------------------------------------------------------------------------------
    # ORM METHODS
    #-------------------------------------------------------------------------------             
    @api.model
    def create(self, values):
        if values.get('posit_id') or values.get('posit_tache_id'):
            if values.get('posit_tache_id'):
                position = self.env['chantier.position'].browse(values.get('posit_tache_id'))
            else:
                position = self.env['chantier.position'].browse(values.get('posit_id'))
            values.update({'order_id':position.order_id.id           
                })
        values.update(self._prepare_add_missing_fields(values))
        line = super(sale_order_line, self).create(values)
        if line.state == 'sale':
            line._action_procurement_create()
            msg = _("Extra line with %s ") % (line.product_id.display_name,)
            line.order_id.message_post(body=msg)

        return line
    
    #-------------------------------------------------------------------------------
    # ONCHANGE METHODS
    #------------------------------------------------------------------------------- 
    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        res = super(sale_order_line, self).product_id_change()
        print "res",res
        if self.product_id:
            context = (self._context or {})
            if context.get('is_position', False) == True:
                if self.product_id.type == 'service': 
                    self.prix_vente_total_unitaire = self.product_id.standard_price
                    self.price_unit = self.product_id.standard_price
                if self.product_id.type != 'service': 
                    self.price_revient = self.product_id.standard_price
                    self.type = self.product_id.type
                    self.type_vente = self.product_id.type_vente
                    self.marge = self.product_id.marge
                    self.frais_usinage_unitaire= self.product_id.frais_usinage 
                    self.frais_livraison_speciaux = self.product_id.frais_livraison_speciaux
                    self.longueur= self.product_id.longueur
                    self.largeur = self.product_id.largeur
                    self.delay= self.product_id.sale_delay
                    self.delay_fabric= self.product_id.produce_delay 
        return res

    @api.multi
    @api.onchange('longueur', 'largeur', 'diametre', 'product_id', 'qty', 'th_weight', 'type_vente')
    def dimension_change(self): 
        area = 0        
        self.prix_achat_unitaire = 0
        if self.type_vente == 'm2' and self.product_id.surface_peindre:
            if self.longueur and self.largeur :
                area = self.longueur * self.largeur   
            if self.diametre:
                area = (self.diametre / 2) * (self.diametre / 2) * math.pi
            area = area / 1000000         
            self.prix_achat_unitaire = self.product_id.standard_price * area / self.product_id.surface_peindre
            self.th_weight = self.product_id.poids_contenance * area / self.product_id.surface_peindre
        if self.type_vente == 'ml' and self.product_id.surface_peindre:
            # le longueur doit etre converti en m
            area = (self.longueur / 1000) * self.product_id.surface_peindre / self.product_id.uos_coeff 
            self.th_weight = self.longueur * self.product_id.poids_contenance / self.product_id.uos_coeff 
            self.prix_achat_unitaire = self.product_id.standard_price * area / self.product_id.surface_peindre
        self.surface_unitaire = area
        if self.type_vente == 'PCE':            
            self.prix_achat_unitaire = self.product_id.standard_price / self.product_id.uos_coeff 
        if self.type_vente == 'kg' and self.product_id.poids_contenance:  
            self.prix_achat_unitaire = self.product_id.standard_price * self.th_weight / self.product_id.poids_contenance

    @api.multi
    @api.onchange('price_unit','frais_usinage_unitaire','frais_livraison_speciaux','qty','prix_achat_unitaire')
    def onchange_price_unit(self):
        marge = 0
        if self.qty > 0 and self.prix_achat_unitaire > 0:
            marge = ((self.price_unit - self.frais_usinage_unitaire - (self.frais_livraison_speciaux / self.qty) - self.prix_achat_unitaire) / self.prix_achat_unitaire) * 100
            prix_vente = self.prix_achat_unitaire * (marge / 100 + 1)
            self.prix_vente_total_unitaire = prix_vente  
            self.total_price = prix_vente * self.qty + self.frais_livraison_speciaux + (self.frais_usinage_unitaire * self.qty)
            self.prix_vente_total = prix_vente * self.qty
        dict(self._context, change_price=1)
        self.marge = marge
    
    @api.multi
    @api.onchange('prix_achat_unitaire','marge','qty','th_weight','surface_unitaire','price_unit','frais_usinage_unitaire','frais_livraison_speciaux')
    def on_change_unit_groupe(self):
        context = (self._context or {}) 
        if context.get('change_price', False) == 1:
            return
        else:            
            prix_vente = self.prix_achat_unitaire * (self.marge / 100 + 1)
            self.prix_vente_total_unitaire = prix_vente
            if self.qty > 0: 
                self.price_unit = prix_vente + self.frais_usinage_unitaire + (self.frais_livraison_speciaux / self.qty)
                self.prix_vente_total = prix_vente * self.qty
            self.purchase_price = self.prix_achat_unitaire * self.qty
            self.th_weight_total = self.th_weight * self.qty
            self.surface_total = self.surface_unitaire * self.qty        
            self.frais_usinage_total = self.frais_usinage_unitaire * self.qty 
            quantity = context.get('qty', False)
            self.product_uom_qty = quantity * self.qty
            if not context.get('change_total', False) == 1:  
                self.total_price = prix_vente * self.qty + self.frais_livraison_speciaux + self.frais_usinage_unitaire * self.qty
        
    @api.multi
    @api.onchange('qty','prix_achat_unitaire','total_price','frais_usinage_total','frais_livraison_speciaux')
    def update_unit_price_marge(self): 
        #Mettre a jour le prix unitaire de vente et le marge      
        if self.qty > 0 and self.prix_achat_unitaire > 0 :
            prix_unitaire_vente = (self.total_price - self.frais_usinage_total - 
                               self.frais_livraison_speciaux) / self.qty
            self.prix_vente_total_unitaire = prix_unitaire_vente
            self.marge = (prix_unitaire_vente / self.prix_achat_unitaire - 1) * 100
        dict(self._context, change_total=1)
    #-------------------------------------------------------------------------------
    # Ces méthodes ne sont pas encore migrés
    #-------------------------------------------------------------------------------   
        
    #créer les lignes de facture d'une commande SER
#     def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
#         res = {}
#         if not line.invoiced:
#             if not account_id:
#                 if line.product_id:
#                     account_id = line.product_id.property_account_income.id
#                     if not account_id:
#                         account_id = line.product_id.categ_id.property_account_income_categ.id
#                     if not account_id:
#                         raise osv.except_osv(_('Error!'),
#                                 _('Please define income account for this product: "%s" (id:%d).') % \
#                                     (line.product_id.name, line.product_id.id,))
#                 else:
#                     prop = self.env['ir.property').get(cr, uid,
#                           property_account_income_categ', 'product.category',
#                             context=context)
#                     account_id = prop and prop.id or False
#             uosqty = self._get_line_qty(cr, uid, line, context=context)
#             uos_id = self._get_line_uom(cr, uid, line, context=context)
#             pu = 0.0
#             if uosqty:
#                 pu = round(line.price_unit * line.product_uom_qty / uosqty,
#                         self.env['decimal.precision').precision_get(cr, uid, 'Product Price'))
# 
#             fpos = line.order_id.fiscal_position or False
#             account_id = self.env['account.fiscal.position').map_account(cr, uid, fpos, account_id)
#             if not account_id:
#                 raise osv.except_osv(_('Error!'),
#                             _('There is no Fiscal Position defined or Income category account defined for default properties of Product categories.'))
#             is_discount = False
#             if line.arrete_b or line.discount_b:
#                 print pu
#                 print line.tax_id
#                 is_discount = True
#             res = {
#               name': line.name,
#               sequence': line.sequence,
#               origin': line.order_id.name,
#               account_id': account_id,
#               price_unit': pu,
#               quantity': uosqty,
#               discount': line.discount,
#               uos_id': uos_id,
#               product_id': line.product_id.id or False,
#               invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
#               account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
#                 #Start
#               is_title':line.is_title,
#               libelle': line.name,
#               discount_b': is_discount,
#               debours_b':line.debours_b,
#               phase_amount':0.0, 
#               advance':100,
#               is_advance':True,
#                 #End             
#             }
#  
#         return res
    
    



