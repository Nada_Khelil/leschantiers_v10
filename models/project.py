# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import odoo.exceptions
import odoo.osv.osv
import odoo.addons.decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)



class consumed_quantity(models.Model): 
    _name = "consumed.quantity"
    _description = "Consumed Quantity"
    
    consumed_qty = fields.Float('Consumed Quantity')
 
    
    def add_consumed_quantity(self):
        context = (self._context or {})
        sale_line_obj = self.env['sale.order.line']
        for qty in self:
            if qty.consumed_qty > 0.0: 
                id = context.get('active_id') or False
                sale_line = sale_line_obj.browse(id)
                if (sale_line.qty_advance + qty.consumed_qty) <= sale_line.product_uom_qty:
                    qty_added = sale_line.qty_advance + qty.consumed_qty
                    sale_line.write({'qty_advance': qty_added})
                else:
                    raise odoo.osv.osv.except_osv(_('Exception!'),
                                _('Quantity consumed must be lower then the quantity of the sale line.'))
            else:
                raise odoo.osv.osv.except_osv(_('Exception!'),
                                _('Quantity consumed must not null and not negative'))
        return {'type': 'ir.actions.act_window_close',}
 

#-------------------------------------------------------------------------------
# project_project
#-------------------------------------------------------------------------------
class project_project(models.Model):
    _inherit = 'project.project'

    def tasks_lines(self, cr, uid, ids, project_id=None, context=None):
        """
        Returns task from a specified project.

        :Parameters:
            -'project_id' (int): specify the concerned project(Chantier).
        """
        task_obj = self.pool.get("project.task")
        tasks=[]
        tasks_lines = task_obj.search(cr, uid,  [('project_id', '=', project_id)], context=context) 
        for task in tasks_lines:
            tasks.append(task_obj.browse(cr, uid, task, context=context))
        return tasks
    
    def _get_acompte_invoice(self, cr, uid, ids, field_name, arg, context=None):
        """Get invoice acompte from project contract"""
        invoice_obj = self.pool['account.invoice']
        res = {}
        for project in self.browse(cr, uid, ids, context=context):
            invoice_ids = invoice_obj.search(cr, uid, [('analytic_id','=',project.analytic_account_id.id),('acompted','=',True)]) 
            res[project.id] = invoice_ids
        return res
    
    def _get_invoice_payment(self, cr, uid, ids, field_name, arg, context=None):
        """Get invoice acompte payments from project contract"""
        invoice_obj = self.pool['account.invoice']
        res = dict([(id, {'payment_ids':[],'payment':0.0}) for id in ids]) 
        payment_ids = []
        payment = 0.0
        for project in self.browse(cr, uid, ids, context=context):
            invoice_ids = invoice_obj.search(cr, uid, [('analytic_id','=',project.analytic_account_id.id)]) 
            for invoice in  invoice_obj.browse(cr, uid, invoice_ids, context=context):
                payment_ids.append( x.id for x in invoice.payment_ids)
                for x in invoice.payment_ids:
                    payment += x.credit   
            res[project.id]['total_payment'] = payment 
            res[project.id]['payment_ids'] = payment_ids
        return res
    
    def _get_total_phase(self, cr, uid, ids, field_name, arg, context=None):
        """Get total phases from project phases"""
        phase_obj = self.pool['project.phase']
        res = {}  
        payment = 0.0
        for project in self.browse(cr, uid, ids, context=context): 
                for phase in  phase_obj.browse(cr, uid, project.phase_ids.ids, context=context): 
                    payment += phase.amount_untaxed_position 
                res[project.id] = payment 
        return res

    def _get_total_project(self, cr, uid, ids, field_name, arg, context=None):
        """Get total phases from project phases"""
        res = {} 
        for project in self.browse(cr, uid, ids, context=context):  
                res[project.id] = project.total_phase - project.total_payment
        return res
    
    def _hours_progress(self, cr, uid, ids, field_names, args, context=None): 
        res = {}
        planned = 0.0 
        effective = 0.0
        for project in self.browse(cr, uid, ids, context=context): 
            for phase in project.phase_ids:
                planned += phase.planned_amount_products_to_receive
                effective += phase.effective_amount_products_to_receive
            if planned != 0.0:
                res[project.id] = round(100.0 * effective/planned) 
            else:
                res[project.id] = 0.0
        return res
    def onchange_partner(self, cr, uid, ids,partner_id,architect_id,context=None):
        project = self.browse(cr, uid, ids,context=context)      
        nb=project.nb_exemplaire
        if partner_id:
            if not project.partner_id:
                nb +=1
        if architect_id:
            if not project.architect_id:
                nb +=1
        return{'value':{'nb_exemplaire':nb}}
        
    def _phase_count(self, cr, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, 0)
        phase_ids = self.pool.get('project.phase').search(cr, uid, [('project_id', 'in', ids),('is_title','=',False)])
        for phase in self.pool.get('project.phase').browse(cr, uid, phase_ids, context):
            res[phase.project_id.id] += 1
        return res

    @api.multi
    def print_contract(self):  
        return self.env['report'].get_action(self, 'leschantiers.report_contract')
    
    def _check_final_inv(self, cr, uid, ids, field, arg, context=None):
        result = {}
        for project in self.browse(cr, uid, ids):
            if project.analytic_account_id:
                cr.execute(
                       '''select count(*) from account_invoice
                       where invoice_type = 'facture_finale' and analytic_id = %s''' 
                       % (project.analytic_account_id.id))
                value = cr.fetchone()[0] or 0
                result[project.id] = value
        return result
    
    _columns = {'invoice_ids': fields.function(_get_acompte_invoice, relation="account.invoice", string="Acompte invoice", type="one2many"),
                'payment_ids': fields.function(_get_invoice_payment, relation="account.move.line", string="Acompte invoice Payment", type="one2many",multi="payment"),
                'total_payment': fields.function(_get_invoice_payment,  string="Acompte invoice Payment", type="float",multi="payment"),
                'total_phase': fields.function(_get_total_phase, string="Total Phases", type="float" ),
                'total_project': fields.function(_get_total_project, string="Total Project", type="float" ),
                'progress_product_receive': fields.function(_hours_progress, string='Product receiving Progress (%)'),
                'position_id': fields.many2one('chantier.position', 'Position',readonly=True),
                'client_id': fields.many2one('res.partner','Maitre d\'ouvrage'),
                'architect_id':fields.many2one('res.partner','Architect'),
                'numero':fields.char('Numéro',readonly=True),
                'garantie_id':fields.many2one('gendre.garantie', 'Genre de garantie'),
                'nb_exemplaire':fields.integer('Nombre d\'exemplaire'),
                'note': fields.html('Note'),
                'phase_count': fields.function(_phase_count, type='integer', string="Open Phases"),
                'task_line': fields.one2many('project.task', 'project_id', string='Tasks'),
                'check_final_inv': fields.function(_check_final_inv,string="Final invoice"),
                }
    _defaults = {  
        'nb_exemplaire': 1, 
        }
    def create(self, cr, uid, vals, context=None):
        seq=self.pool.get('ir.sequence').get(cr,uid,'project.project',context=context)
        vals['numero']=str(seq)
        return super(project_project, self).create(cr, uid, vals, context=context)
    
    def create_open_invoices(self, cr, uid, ids, context=None):
        project = self.browse(cr, uid, ids[0], context) 
        if project.use_phases:
            type = 'situation'
        fiscal_pos_obj = self.pool.get('account.fiscal.position')
        mod_obj = self.pool.get('ir.model.data')
        inv_values = {
                    'invoice_type':type,
                    'name': 'Construction '  + project.name,
                    'partner_id': project.partner_id.id,
                    'company_id': project.company_id.id,
                    'date_invoice' : date.today(),
                    'analytic_id' : project.analytic_account_id.id,
                    'payment_term': project.partner_id.property_payment_term.id or False,
                    'account_id': project.partner_id.property_account_receivable.id,
                    'currency_id': project.pricelist_id.currency_id.id, 
                    'fiscal_position': project.partner_id.property_account_position.id,
                    'from_construction_project': True, 
                    'project_id': project.id,  
                    } 
        inv_id = self.pool.get('account.invoice').create(cr, uid, inv_values, context=context)
        if project.use_phases:
            for phase in project.phase_ids:
                tax = fiscal_pos_obj.map_tax(cr, uid, project.partner_id.property_account_position, [])
                curr_line = {'price_unit': (phase.total * phase.advance) /100,
                            'quantity': phase.posit_id.qty,
                            'name': phase.posit_id.name,
                            'product_id': False, 
                            'invoice_id': inv_id, 
                            'uos_id': 1 or False, 
                            'invoice_line_tax_id': [(6,0,tax)],
                            'planned':phase.total,
                            'effective':(phase.total * phase.advance) /100,
                            'progress': phase.advance,
                            'phase_id':phase.id,
                        }
            self.pool.get('account.invoice.line').create(cr, uid, curr_line, context=context)
        else:
            for mission in project.mission_budget_ids:
                tax = fiscal_pos_obj.map_tax(cr, uid, project.partner_id.property_account_position, [])
                miss_line = {
                            'price_unit': mission.price_unit,
                            'quantity': mission.product_qty,
                            'name': mission.product_id.name,
                            'product_id': mission.product_id.id, 
                            'invoice_id': inv_id, 
                            'uos_id': 1 or False, 
                            'invoice_line_tax_id': [(6,0,tax)],
                            'planned':mission.product_qty,
                            'effective':mission.avancement,
                            'progress': mission.pourcentage,
                        }
                self.pool.get('account.invoice.line').create(cr, uid, miss_line, context=context)
            for prod in project.mission_furniture_ids:
                tax = fiscal_pos_obj.map_tax(cr, uid, project.partner_id.property_account_position, [])
                prod_line = {
                            'price_unit': prod.price_unit,
                            'quantity': prod.product_qty,
                            'name': prod.product_id.name,
                            'product_id': prod.product_id.id, 
                            'invoice_id': inv_id, 
                            'uos_id': 1 or False, 
                            'invoice_line_tax_id': [(6,0,tax)],
                            'planned':prod.product_qty,
                            'effective':prod.avancement,
                            'progress': prod.pourcentage,
                        }
                self.pool.get('account.invoice.line').create(cr, uid, prod_line, context=context)
        res = mod_obj.get_object_reference(cr, uid, 'account', 'invoice_form')
        res_id = res and res[1] or False,
        return {
            'name': _('Project Invoice'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'res_id': inv_id,  
            'view_id': [res_id] or False,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
        }
        
#-------------------------------------------------------------------------------
# project_phase
#-------------------------------------------------------------------------------
class project_phase(osv.osv):
    _inherit = "project.phase"
    _order = 'sequence'
    
    def _get_projects_from_tasks(self, cr, uid, task_ids, context=None):
        tasks = self.pool.get('project.task').browse(cr, uid, task_ids, context=context)
        phase_ids = [task.phase_id.id for task in tasks if task.phase_id]
        return self.pool.get('project.phase')._get_project(cr, uid, phase_ids, context)
 
    def _get_project(self, cr, uid, ids, context=None):
        """ return the project ids and all their parent projects """
        result = {}
        for phase in self.pool.get('project.phase').browse(cr, uid, ids, context=context):
            result[phase.id] = True
        return result.keys()
    
    def _progress_product_rate(self, cr, uid, ids, names, arg, context=None): 
        res = {}
        for phase in self.browse(cr, uid, ids, context=context):
            planned = effective = total = 0.0 
            res[phase.id] = {
                'planned_amount_products_to_receive': 0.0,
                'effective_amount_products_to_receive': 0.0, 
                'product_total': 0.0,
                'total_phase': 0.0,
            } 
#             for purchase_line in phase.purchase_line_ids:
#                 stock_ids = self.pool.get('stock.move').search(cr, uid, [('purchase_line_id', '=', purchase_line.id),('state','=','done')])
#                 for stock in self.pool.get('stock.move').browse(cr, uid, stock_ids, context=context):
#                     effective += stock.product_uom_qty*purchase_line.price_unit
#             effective += purchase_line.price_subtotal 
            for sale_line in phase.position_id.position_line_ids:
                if sale_line.product_id.type != 'service': 
                    planned += sale_line.product_uom_qty
                    effective += sale_line.qty_advance
                    total += sale_line.price_subtotal 
            res[phase.id]['planned_amount_products_to_receive'] = planned
            res[phase.id]['effective_amount_products_to_receive'] = effective
            res[phase.id]['product_total'] = total
            res[phase.id]['total_phase'] = phase.amount_untaxed_effectif_position - total
        return res
    
    def _hours_amount_position(self, cr, uid, ids, names, arg, context=None): 
        res = dict([(id, {'amount_untaxed_position':0.0,'amount_untaxed_effectif_position':0.0, 'position_amount_percent':0.0}) for id in ids])
        chantier_position_obj = self.pool.get('chantier.position')
        for phase in self.browse(cr, uid, ids, context=context): 
            position_id = chantier_position_obj.search(cr, uid, [('id', '=', phase.position_id.id)])
            if position_id:
                position = chantier_position_obj.browse(cr, uid, position_id, context=context)
                res[phase.id]['amount_untaxed_position'] = position.total_untaxed
                res[phase.id]['amount_untaxed_effectif_position'] = position.amount_total_ht_effectif
                res[phase.id]['position_amount_percent'] = position.total_untaxed*phase.progress_by_user/100
        return res

    def _totals(self, cr, uid, ids, names, arg, context=None): 
        res = dict([(id, {'total_amount_product':0.0,'total_amount_task':0.0, 'progress_total':0.0}) for id in ids])
        chantier_position_obj = self.pool.get('chantier.position')
        total_amount_product = total_amount_task = progress_total = progress_task = 0.0
        number = 0
        for phase in self.browse(cr, uid, ids, context=context): 
            position_id = chantier_position_obj.search(cr, uid, [('id', '=', phase.position_id.id)])
            if position_id:
                for task in phase.task_ids:
                    number += 1
                    progress_task += task.progress
                position = chantier_position_obj.browse(cr, uid, position_id, context=context)
                for line in position.position_line_ids:
                    if line.product_id.type == 'service': 
                        total_amount_task+=line.price_subtotal
                    if line.product_id.type != 'service': 
                        total_amount_product+=line.price_subtotal
                progress_total = ((100.0 *phase.effective_amount_products_to_receive/phase.planned_amount_products_to_receive) + progress_task/number )/2
                res[phase.id]['total_amount_product'] = total_amount_product
                res[phase.id]['total_amount_task'] = total_amount_task
                res[phase.id]['progress_total'] = progress_total
        return res
    
    def _get_sale_order(self, cr, uid, ids, field_name, arg, context=None):
        """Get sale order for courant phase"""
        sale_line_obj = self.pool.get('sale.order.line')
        route_obj = self.pool.get('stock.location.route')
        res = {}
        sale_line_ids = [] 
        line_ids = [] 
        route_ids = []
        mto_route_id = route_obj.search(cr, uid, [('name', 'like', _('Make To Order'))], context=context)
        m_route_id = route_obj.search(cr, uid, [('name', 'like', _('Manufacture'))], context=context)
        manufacture_mto = True
        if m_route_id:
            route_ids.append(m_route_id[0])
            if mto_route_id:
                route_ids.append(mto_route_id[0])
        for phase in self.browse(cr, uid, ids, context=context):
            if phase.contract_id:
                sale_line_ids = sale_line_obj.search(cr, uid, [('posit_id','=',phase.position_id.id)]) 
                for sale_line in sale_line_obj.browse(cr, uid, sale_line_ids, context=context):
                    b = sale_line.product_id.route_ids.ids
                    a = route_ids not in sale_line.product_id.route_ids.ids
                    for route in route_ids:
                        if route not in sale_line.product_id.route_ids.ids:
                            manufacture_mto = False
                    if sale_line.product_id.type != 'service': 
#                          and manufacture_mto == False
                        line_ids.append(sale_line.id)
#                     manufacture_mto = True
            res[phase.id] = line_ids
        return res
    
    def _progress_get(self, cr, uid, ids, field_names, args, context=None): 
        res = {}
        for phase in self.browse(cr, uid, ids, context=context): 
            if phase.planned_amount_products_to_receive != 0:
                res[phase.id] = round(100.0 * phase.effective_amount_products_to_receive/phase.planned_amount_products_to_receive) 
            else:
                res[phase.id] = 0.0
        return res
    
    def _total_mo_mp(self, cr, uid, ids, field_names, args, context=None): 
        res = {}
        mp = 0.0
        mo = 0.0
        for phase in self.browse(cr, uid, ids, context=context):
            for line in phase.materials_ids:
                mp +=line.product_qty * line.price_unit
            for line in phase.oeuvre_ids:
                mo +=line.product_qty * line.price_unit
            res[phase.id]={'total_mo':mo,'total_mp':mp,'total':mo+mp}
        return res        
    
    def onchange_progress_by_user(self, cr, uid, ids,progress_by_user,context=None): 
        res = {'value':{}}  
        if progress_by_user:
            if ids:
                phase = self.pool.get('project.phase').browse(cr,uid,ids[0]) 
                res['value']['position_amount_percent'] = phase.amount_untaxed_position*progress_by_user/100
        return res 
     
    def onchange_advance_wiz(self, cr, uid, ids, advance_wiz, context=None): 
        phase = self.browse(cr, uid, ids, context=context) 
        amount =  (phase.sale_amount * advance_wiz) /100 or 0.0
        return {'value': {'total_advance' : amount}}
    
    _columns = {
        'contract_id': fields.many2one('account.analytic.account', 'Contract'),
        'product_uom': fields.many2one('product.uom', 'Duration Unit of Measure',  help="Unit of Measure (Unit of Measure) is the unit of measurement for Duration", states={'done':[('readonly',True)], 'cancelled':[('readonly',True)]}),
        'libelle': fields.html('Libellé'),
        'position_id': fields.many2one('chantier.position', 'Chantier position'),
        'amount_untaxed_position': fields.function(_hours_amount_position, digits_compute=dp.get_precision('Account'), multi="hours",string='Montant Position'),
        'position_amount_percent': fields.function(_hours_amount_position, digits_compute=dp.get_precision('Account'), multi="hours",string='Pourcentage'),
        'amount_untaxed_effectif_position': fields.function(_hours_amount_position, digits_compute=dp.get_precision('Account'), multi="hours",string='Montant Position Effectif'),
        #'purchase_line_ids': fields.function(_get_purchase_order, relation="purchase.order.line", string="Phases Sale Lines", type="one2many"),
        'order_line_ids': fields.function(_get_sale_order, relation="sale.order.line", string="Phases Purchase Lines", type="one2many"),
        'planned_amount_products_to_receive': fields.function(_progress_product_rate, multi="progress", string='Planned Products to receive', help="Sum of planned hours of all tasks related to this phase",),
        'effective_amount_products_to_receive': fields.function(_progress_product_rate, multi="progress", string='Effective Products received', help="Sum of planned hours of all tasks related to this phase",),
        'product_total': fields.function(_progress_product_rate, multi="progress", string='Temps passé', help="Total Phase",
        store = { 
                'project.phase': (_get_project, ['task_ids'], 10),
                'project.task': (_get_projects_from_tasks, ['work_ids', 'effective_hours', 'planned_hours','state', 'stage_id'], 20),
            }),
        'total_phase': fields.function(_progress_product_rate, multi="progress", string='Temps passé', help="Total Phase",
        store = { 
                'project.phase': (_get_project, ['task_ids'], 10),
                'project.task': (_get_projects_from_tasks, ['work_ids', 'effective_hours', 'planned_hours','state', 'stage_id'], 20),
            }),
        'progress_product_receiving': fields.function(_progress_get, string='Working Time Progress (%)'),
        'total_amount_product': fields.function(_totals, string='Amount total products',multi="total"),
        'total_amount_task': fields.function(_totals, string='Amount Total tasks',multi="total"),
        'progress_total': fields.function(_totals, string='Working Progress',multi="total"),
        'oeuvre_ids':fields.one2many('phase.manpower','phase_id','Main d Oeuvre'),
        'materials_ids':fields.one2many('phase.materials','phase_id','Matière Première'),
        'total_mo': fields.function(_total_mo_mp, method=True, type='float', string='Total MO Prévu',multi='pahse'),
        'total_mp': fields.function(_total_mo_mp, method=True, type='float', string='Total MP Prévu',multi='pahse'),
        'total':fields.function(_total_mo_mp, method=True, type='float', string='Total Prévu',multi='pahse'),
        'posit_id':fields.many2one('chantier.position', 'Position'),
        'type_phase':fields.selection([
                    ('normal', 'Normale'),
                    ('avenant', 'Avenant'),
                     ], 'Type phase', select=True, default='normal'),
        }





#-------------------------------------------------------------------------------
# project_task
#-------------------------------------------------------------------------------    
class project_task_inherited(osv.osv):
    _inherit = 'project.task'
    

    def _action_count_stock(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for task in self.browse(cr, uid, ids, context=context):
            if task.order_id.procurement_group_id.id :
                cr.execute(
                    "select count (*) from stock_picking where group_id  =  %s " % (task.order_id.procurement_group_id.id))
                value = cr.fetchone()[0] or 0
                res[task.id] = value
            return res
        
    def act_stock_picking(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        for task in self.browse(cr, uid, ids, context=context):
            if task.order_id.procurement_group_id.id :
                requete="select sp.id from stock_picking sp \
                            where sp.group_id  =  '%s'  "% str(task.order_id.procurement_group_id.id)
                cr.execute(requete)
                res = cr.fetchall()
     
             
                data_obj = self.pool.get('ir.model.data')
                id1 = data_obj._get_id(cr, uid, 'stock', 'vpicktree')            
                if id1:
                    id1 = data_obj.browse(cr, uid, id1, context=context).res_id
                id2 = data_obj._get_id(cr, uid, 'stock', 'view_picking_form')
                if id2:
                    id2 = data_obj.browse(cr, uid, id2, context=context).res_id            
         
                value = {
                         
                        'context': context,             
                        'view_type': 'form', 
                        'view_mode': 'tree,form ', 
                        'res_model': 'stock.picking', 
                        'view_id': False, 
                        'views': [(id1, 'tree'),(id2,'form')], 
                        'domain':[('id','in',res)],
                        'type': 'ir.actions.act_window', 
                            }
                 
                return value 
        
    _columns = {
                'action_count_stock': fields.function(_action_count_stock, type='integer'),

                }
#-------------------------------------------------------------------------------
# Phase Manpower
#-------------------------------------------------------------------------------



class project_main_oeuvre(osv.osv):
    _inherit = "mission.budget" 
    _order = 'name'
    
    def _nb_heures_done(self, cr, uid, ids, names, arg, context=None):
        result = {}
        for rec in self.browse(cr, uid, ids, context):
            if rec.phase_id and rec.project_id.use_phases:
                cr.execute('''select sum(unit_amount) from hr_analytic_timesheet hat
                inner join account_analytic_line  aac on aac.id = hat.line_id
                inner join resource_resource rr on rr.user_id = aac.user_id
                inner join hr_employee he on he.resource_id = rr.id
                inner join project_task pt on pt.id = hat.task_id
                inner join project_phase pp on pp.id = pt.phase_id
                where aac.account_id = %s
                and pp.id = %s
                and aac.product_id = %s''' % (rec.phase_id.project_id.analytic_account_id.id, rec.phase_id.id,rec.product_id.id))
                nb_heure_done = cr.fetchone()[0]
            else:
                cr.execute('''select sum(unit_amount) from hr_analytic_timesheet hat
                    inner join account_analytic_line  aac on aac.id = hat.line_id
                    inner join resource_resource rr on rr.user_id = aac.user_id
                    inner join hr_employee he on he.resource_id = rr.id
                    where aac.account_id = %s
                    and aac.product_id = %s''' % (rec.project_id.analytic_account_id.id, rec.product_id.id))
                nb_heure_done = cr.fetchone()[0]
            ecart = 0
            honoraire = 0
            honoraire = rec.price_unit * rec.product_uom_qty
            if nb_heure_done and nb_heure_done != 0 :
                pourcentage = (nb_heure_done * 100) / rec.product_uom_qty
                avancement = nb_heure_done * rec.price_unit
                if rec.price_unit != 0 :
                    ecart = avancement - honoraire 
            else :
                pourcentage = 0
                avancement = 0
            depacement = 0
            if nb_heure_done > rec.product_uom_qty :
                depacement = nb_heure_done - rec.product_uom_qty 
            result[rec.id] = {'nb_heure_done': nb_heure_done, 
                              'pourcentage':pourcentage, 
                              'depacement':depacement , 
                              'avancement':avancement, 
                              'ecart':ecart, 
                              'honoraire':honoraire }
        return result

    _columns = {'nb_heure_done': fields.function(_nb_heures_done, method=True, multi="houres_done", type='float', string='Nb.done'),
                'depacement': fields.function(_nb_heures_done, method=True, type='float', multi="houres_done", string='Overtaking'),
                'avancement': fields.function(_nb_heures_done, method=True, type='float', multi="houres_done", string='Progress'),
                'ecart': fields.function(_nb_heures_done, method=True, type='float', multi="houres_done", string='Difference'),}

class phase_manpower(osv.osv):
    _name = "phase.manpower"
    
    def _nb_heures_done(self, cr, uid, ids, names, arg, context=None):
        result = {}
        for rec in self.browse(cr, uid, ids, context):
            cr.execute('''select sum(unit_amount) from hr_analytic_timesheet hat
                inner join account_analytic_line  aac on aac.id = hat.line_id
                inner join resource_resource rr on rr.user_id = aac.user_id
                inner join hr_employee he on he.resource_id = rr.id
                where aac.account_id = %s
                and he.product_id = %s
                and aac.phase_id = %s''' % (rec.phase_id.project_id.analytic_account_id.id, rec.product_id.id,rec.phase_id.id))
            nb_heure_done = cr.fetchone()[0]
            result[rec.id] = nb_heure_done
        return result
    
    _columns = {
            'product_id' : fields.many2one('product.product',  'Qualification'),
            'product_qty' : fields.float("Heures Prévus"), 
            'hours_done': fields.function(_nb_heures_done, method=True, type='float', string='Hours Done'),
            'product_uom' :fields.many2one('product.uom',  'Unité'),
            'price_unit': fields.float('Prix Unité'), 
            'phase_id':fields.many2one('project.phase', string="Phase"),
            'project_id':fields.many2one('project.project', string="Projet"),
            'origin':fields.many2one('sale.order.line','Origine')
    }
    
#-------------------------------------------------------------------------------
# Phase Materials
#-------------------------------------------------------------------------------
class phase_materials(osv.osv):
    _name = "phase.materials"  
    _columns = {
            'product_id' : fields.many2one('product.product',  'Produit'),
            'product_qty' : fields.float("Quantité Prévu"),   
            'product_uom' :fields.many2one('product.uom',  'Unité de Mesure'),
            'price_unit': fields.float('Prix Unité'), 
            'phase_id':fields.many2one('project.phase', string="Phase"),
            'project_id':fields.many2one('project.project', string="Projet"),
            'origin':fields.many2one('sale.order.line','Origine')
    }
    
    
