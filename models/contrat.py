# -*- coding: utf-8 -*- 
from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp

#-------------------------------------------------------------------------------
# account
#-------------------------------------------------------------------------------
class account(models.Model):
    _inherit = "account.analytic.account"
 
    #Compter les positions liées au contrat chantier
    def _position_count(self): 
        for account in self:
            self.position_count = self.env['chantier.position'].search_count([('position_id.parent_id','=', account.id)])

 
    def _get_account_discount(self):
        """Get invoice acompte payments from contract analytic"""
        sale_obj = self.env['sale.order']
        res = {} 
        discount_ids =[]  
        add_to_discount = False
        for analytic in self:
#             res[analytic.id]['payment_ids'] =  []
            sale_ids = sale_obj.search([('project_id','=',analytic.id)])  
            if sale_ids:
                for sale in sale_obj.browse(sale_ids):
                    if sale.discount_ids and len(sale.discount_ids)>0: 
                        for x in sale.discount_ids:
                            if x.discount_method == 'percentage':
                                add_to_discount = True
                                for discount in self.env['sale.discount.line'].browse(discount_ids):
                                    if discount.discount_method == 'percentage' and discount.discount_pr == x.discount_pr:
                                        add_to_discount = False
                                if add_to_discount == True:
                                    discount_ids.append( x.id ) 
#                             if x.discount_method == 'fixed':
#                                 add_to_discount = True
#                                 for discount in self.pool.get('sale.discount.line').browse(cr, uid, discount_ids, context=context):
#                                     if discount.discount_method == 'fixed' and discount.price_unit == x.price_unit:
#                                         add_to_discount = False
#                                 if add_to_discount == True:
#                                     discount_ids.append( x.id )          
                self.discount_ids = discount_ids 
              
    def _get_position(self):
        """Get account position for sale orders"""
        sale_obj = self.pool['sale.order']
        position_ids =[]   
        for analytic in self:
#             res[analytic.id]['payment_ids'] =  []
            sale_ids = sale_obj.search([('project_id','=',analytic.id)])  
            if sale_ids:
                for sale in sale_obj.browse(sale_ids):
                    if sale.position_line and len(sale.position_line)>0: 
                        for x in sale.position_line: 
                            position_ids.append( x.id )         
            self.position_line = position_ids 
     
    ############Ajouter par Rahma
    def _get_position_offre(self):
        """Get account position for sale orders"""
        for account in self: 
            pos_ids = self.pool['chantier.position'].search([('position_id.parent_id','=',account.id)])          
            self.position_offre_line = pos_ids  
    
    def _get_demanded(self):
        invoice_obj = self.env['account.invoice']
        demanded = 0.0 
        for account in self:
            invoice_ids = invoice_obj.search([('analytic_id', '=', account.id), ('acompted', '=', True),('state', 'not in', ['draft', 'cancel'])])
            for analytic in self:
                invoices = invoice_obj.browse(invoice_ids)
                for inv in invoices:
                    demanded += inv.amount_total or 0.0
            self.demanded = demanded

    
    def _get_recieved(self):
        invoice_obj = self.env['account.invoice']
        recieved = 0.0        
        for analytic in self:
            invoice_ids = invoice_obj.search([('analytic_id', '=', analytic.id), ('acompted', '=', True),('state', 'not in', ['draft', 'cancel'])])
            invoices = invoice_obj.browse(invoice_ids)
            for inv in invoices:
                for pay in inv.payment_ids:
                    recieved += pay.credit or 0.0
            self.recieved = recieved
    
    def _get_inv_total(self): 
        invoice_obj = self.env['account.invoice']
        for account in self:
            payment = amount_situation = amount_final= total=0
            invoice_ids = invoice_obj.search([('analytic_id','=',account.id),('state', 'not in', ['draft', 'cancel'])]) 
            for invoice in  invoice_obj.browse(invoice_ids):
                for x in invoice.payment_ids:
                    payment += x.credit 
                if invoice.invoice_type == 'facture_finale':
                    amount_final = invoice.amount_total_invoice
                elif invoice.invoice_type == 'situation':
                    amount_situation += invoice.situation_amount
            if amount_final > 0:
                total = amount_final
            else :
                total = amount_situation + account.demanded
            account.update({
                   'total_inv': total,
                   'total_pay':payment
                })
              
    num_position = fields.char('Numéro position')
    position_count = fields.Integer(compute='_position_count', string="Positions")
    position_line_ids = fields.One2many('position.modele.line','account_id', string='Positions') #pour position dans sale order, a comme id account_id
    position_line = fields.One2many(compute='_get_position',relation="chantier.position", string="Account Positions")
    position_offre_line = fields.One2many(compute='_get_position_offre' ,relation="chantier.position", string="Positions dans l'offre")
    libelle = fields.Html(string='Libellé')
    chantier_position_id = fields.Many2one('chantier.position', string='Chantier position') #contract par position
    discount_ids = fields.One2many(compute='_get_account_discount',relation="sale.discount.line", string="Discount on account", type="One2many" )
    partner_id = fields.Many2one('res.partner', string='Adjudicataire')
    demanded = fields.Float(compute='_get_demanded' , string="Amount Provision", digits=(16, 2))
    recieved = fields.Float(compute='_get_recieved' , string="Amount Recieved", digits=(16, 2))
    total_inv = fields.Float(compute='_get_inv_total',string="Total invoices")
    total_pay = fields.Float(compute='_get_inv_total',string="Total payment recieved")      
     
    #Bon d'achat pour contrat courant
    def open_positions(self):
        position_ids = self.env['chantier.position'].search([('position_id.parent_id','=', self.id)])
        names = [record.name for record in self]
        name = _('Positions du %s') % ','.join(names)
        return {
                'type': 'ir.actions.act_window',
                'name': name,
                'view_type': 'form',
                'view_mode': 'tree,form',
                'domain' : [('id','in',position_ids)],
                'res_model': 'chantier.position',
                'nodestroy': True,
                } 
                            
            

