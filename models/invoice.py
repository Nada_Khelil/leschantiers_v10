# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime, timedelta
import time
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import netsvc
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp 
from openerp import api


#-------------------------------------------------------------------------------
# invoice
#-------------------------------------------------------------------------------
class invoice(osv.osv):
    _inherit = "account.invoice.line"  
    
    def onchange_progress(self, cr, uid, ids,planned,progress,context=None): 
        res = {'value':{'price_unit' : planned*progress/100.0}}
        return res 
    
    _columns = {   
                'phase_id':fields.many2one('project.phase','Phase' ),  
                'planned':fields.float('Planned', digits_compute=dp.get_precision('Account')),
                'effective':fields.float('Effective', digits_compute=dp.get_precision('Account')),
                'progress':fields.float('Progress', digits_compute=dp.get_precision('Account')),                
                }

    
#-------------------------------------------------------------------------------
# account_invoice
#-------------------------------------------------------------------------------
class account_invoice(osv.osv):
    _inherit = "account.invoice" 
    
    def getsale_id(self, cr, uid, ids, invoice_id=None, context=None):  
        sale_obj = self.pool.get('sale.order')
        account_id = self.browse(cr, uid, invoice_id, context=context).analytic_id.id
        sale_ids = sale_obj.search(cr,uid,[('project_id','=',account_id)])
        if sale_ids:
            return sale_obj.browse(cr,uid,sale_ids[0],context)
    
    def _compute_amount_inv(self, cr, uid, ids, field_name, arg, context=None):
        res = {}  
        for invoice in self.browse(cr, uid, ids, context=context):
            res[invoice.id] = {'untaxed_amount_inv':0,'amount_total_inv':0} 
            rplp = tax = 0.0 
            if invoice.rplp > 0 :
                rplp = invoice.rplp
            else :
                rplp = invoice.amount_rplp 
            if invoice.taxes >0 :
                tax = invoice.taxes
            else :
                tax = invoice.amount_tax                 
            untaxed_amount =  invoice.amount_untaxed_sans_remise - invoice.discount + invoice.debours
            amount_total =  untaxed_amount + rplp + tax 
            res[invoice.id]['untaxed_amount_inv'] = untaxed_amount
            res[invoice.id]['amount_total_inv'] = amount_total
            res[invoice.id]['arrete'] = amount_total - invoice.amount_total
        return res
    
    _columns = { 
                'project_id':fields.many2one('project.project','Project' ),  
                'from_construction_project': fields.boolean('Invoice from Construction Project'), 
                'untaxed_amount_inv':fields.function(_compute_amount_inv,digits_compute=dp.get_precision('Account'),
                                              multi="ser",string='Total HT', help="Invoice untaxed amount",
                                              track_visibility='always'),  
                'amount_total_inv':fields.function(_compute_amount_inv,digits_compute=dp.get_precision('Account'),
                                              multi="ser",string='Total facture TTC', help="Invoice total amount",
                                              track_visibility='always'), 
                'arrete':fields.function(_compute_amount_inv,digits_compute=dp.get_precision('Account'),
                                              multi="ser",string='Arrete',
                                              track_visibility='always'), 
             
                }



    @api.multi  
    def print_construction_situation(self):  
        return self.env['report'].get_action(self,'leschantiers.report_situation')
    
    