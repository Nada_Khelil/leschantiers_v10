# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp



 
#-------------------------------------------------------------------------------
# mt_validite_offre
#-------------------------------------------------------------------------------
class mt_validite_offre(models.Model):
    _name = 'mt.validite_offre'
    _descripion = 'validite_offre'
    
    name = fields.Char('Durée',required=True,translate=True,size=200)
    description = fields.Text('Description', size=64)
                

    
#-------------------------------------------------------------------------------
# product_product
#-------------------------------------------------------------------------------
class product_product(models.Model):

    _name ="delivery.deadline"

    name = fields.Char("Délai de livraison",required=True,size=100)
    
#-------------------------------------------------------------------------------
# sale_discount
#-------------------------------------------------------------------------------
# class sale_discount(models.Model):
#     
#     _inherit = 'sale.discount.line'
#     from_contract = fields.Boolean('Discout from Contract Reference' )
    
#-------------------------------------------------------------------------------
# leschantiers_configuration
#-------------------------------------------------------------------------------
# class leschantiers_configuration(models.TransientModel):
#     _inherit = 'sale.config.settings'
#  
#     def default_get(self, cr, uid, fields, conText=None):
#         ir_model_data = self.pool.get('ir.model.data')
#         res = super(leschantiers_configuration, self).default_get(cr, uid, fields, conText) 
#         res['group_template_required'] = True
#         res['group_analytic_accounting'] = True
#         return res 
     
# #-------------------------------------------------------------------------------
# # leschantiers_configuration_project
# #-------------------------------------------------------------------------------
# class leschantiers_configuration_project(osv.osv_memory):
#     _inherit = 'project.config.settings'
# 
#     def default_get(self, cr, uid, fields, conText=None):
#         ir_model_data = self.pool.get('ir.model.data')
#         res = super(leschantiers_configuration_project, self).default_get(cr, uid, fields, conText) 
#         res['group_time_work_estimation_tasks'] = True
#         res['module_project_timesheet'] = True
#         return res 
# 
# #-------------------------------------------------------------------------------
# # account_config_settings
# #-------------------------------------------------------------------------------
# class account_config_settings(osv.osv_memory):
#     _inherit = 'account.config.settings'
#       
#     def default_get(self, cr, uid, fields, conText=None):
#         ir_model_data = self.pool.get('ir.model.data')
#         res = super(account_config_settings, self).default_get(cr, uid, fields, conText) 
#         res['group_analytic_account_for_sales'] = True
#         return res 

        
#-------------------------------------------------------------------------------
# Client
#-------------------------------------------------------------------------------    
class res_partner_inherited(models.Model):
    _inherit = "res.partner"   

    
    type = fields.Selection([('default', 'Default'), ('invoice', 'Invoice'),
                                   ('delivery', 'Shipping'), ('contact', 'Contact'),
                                   ('intervention', 'Intervention'), ('other', 'Other')], 
                            string='Address Type',default='default',help="Used to select automatically the right address according to the conText in sales and purchases documents.")
    architect = fields.Boolean("Architect")

class product_template(models.Model):     
    _inherit = "product.template"
    
    is_quality=fields.Boolean('Status')
