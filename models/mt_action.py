# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
import time
from datetime import datetime


#-------------------------------------------------------------------------------
# ActionClass
#-------------------------------------------------------------------------------
class ActionClass(osv.osv):
    _inherit = "crm.helpdesk"

    def onchange_contract_id(self, cr, uid, ids,contract_id, context=None):
        if context is None:
            context = {}
        if(context.get('contract_change')):
            res = {}
            project_id = self.pool.get('project.project').search(cr,uid,[('analytic_account_id','=',contract_id)])
            res = {'value': {'project_id': project_id,}}                 
            return res

    def onchange_project_id(self, cr, uid, ids,project_id, context=None):
        if context is None:
            context = {}
        if(context.get('project_change')):
            res = {}
            project= self.pool.get('project.project').browse(cr,uid,project_id)
            res = {'value': {'contract_id': project.analytic_account_id,'partner_id': project.partner_id}}                 
            return res
          
    _columns = {
        'project_id': fields.many2one('project.project', string="Project"),
               }