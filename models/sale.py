# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import odoo.exceptions
import odoo.osv.osv
import odoo.addons.decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)

# #-------------------------------------------------------------------------------
# # genre_garantie
# #-------------------------------------------------------------------------------
class genre_garantie(models.Model):
    _name = "genre.garantie"
    name = fields.Char("Name", size=100)
    
# #-------------------------------------------------------------------------------
# # sale_order
# #-------------------------------------------------------------------------------
class sale_order(models.Model):
    _inherit = "sale.order" 
    
    #-------------------------------------------------------------------------------
    # FIELDS
    #-------------------------------------------------------------------------------     
    order_line_no_title = fields.One2many('sale.order.line', 'order_id', 'Order Lines', domain=[("is_title", "=", False)])
    order_line_create_task = fields.One2many('sale.order.line', 'order_id', 'Order Lines', domain=[("create_task", "=", True)])
    objet = fields.Char("Objet", size=100)
    position_line = fields.One2many('chantier.position', 'order_id', domain=[('is_title', '=', False)], ondelete='cascade', 
                                    string='Positions', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=True)
    position_line_title = fields.One2many('chantier.position', 'order_id', ondelete='cascade', string='Positions', 
                                          readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=True)
    delai_id = fields.Many2one('delivery.deadline', 'Delivery deadline')
    votre_client = fields.Char('Votre client')
    total_bkp_chant = fields.Float('Total Arrêté', digits=dp.get_precision('Product Price'))
    new_note = fields.Text('Offre note')
    name_char = fields.Char(string='', default='A',size=3, readonly=True, 
                            states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    partner_contact_id = fields.Many2one('res.partner', 'Contact', 
                                         states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, 
                                         help="Contact partner for current sales order.")
    pos_count = fields.Integer(compute='_count', string='Positions')
    pos_count2 = fields.Integer(compute='_count', string='Positions')
    ch_count = fields.Integer(compute='_count',string='CH')
    #ser_count = fields.Integer(compute='_count',string='SER')
    use_positions = fields.Boolean('Use position',default=True)
    interlocutor_id = fields.Many2one('res.partner', string='Interlocutor', readonly=True, 
                                      states={'draft': [('readonly', False)], 'sent': [('readonly', False)]})
    #fiscalyear_id = fields.Many2one('account.fiscalyear', string='Exercice', default=_default_fiscal_year)
    type_offre = fields.Selection([
                    ('principale', 'Principale'),
                    ('avenant', 'Avenant'),
                     ], 'Type Offre', select=True, default='principale')
    offre_id = fields.Many2one('sale.order', 'Offre Principal')
    adresse_intervention_id = fields.Many2one('res.partner', 'Adresse Intervention')
    validite_id = fields.Many2one('mt.validite_offre', 'Offer validity')
    is_client = fields.Selection([('client', 'Client'), ('org', 'Organisation')], 'Type')
    client_code = fields.Boolean('Client',default=False)
    margin_sale = fields.Float(compute='_product_margin', string='Margin', store=True,readonly=True,
                               digits=dp.get_precision('Product Price'))
    percentage_margin = fields.Float(compute='_product_margin', string='Margin(%)', store=True,readonly=True, 
                                     multi="all", digits=dp.get_precision('Product Price'))
    net_mp = fields.Float(compute='_product_margin', string='Total matière première', store=True,readonly=True, 
                          multi="all", digits=dp.get_precision('Product Price'))
    percentage_net = fields.Float(compute='_product_margin', string='Matière première (%)', store=True,readonly=True, 
                                  multi="all", digits=dp.get_precision('Product Price'))
    net_fab = fields.Float(compute='_product_margin', string="Total main d'oeuvre", store=True,readonly=True, 
                           multi="all", digits=dp.get_precision('Product Price'))
    percentage_fab = fields.Float(compute='_product_margin', string="Main d'oeuvre (%)", store=True,readonly=True, 
                                  multi="all", digits=dp.get_precision('Product Price'))
    total_frais = fields.Float(compute='_product_margin', string="Total frais", store=True,readonly=True, 
                               multi="all", digits=dp.get_precision('Product Price'))
    percentage_frais = fields.Float(compute='_product_margin', string="Frais (%)", store=True,readonly=True, 
                                    multi="all", digits=dp.get_precision('Product Price'))
    project_id = fields.Many2one('account.analytic.account', 'Chantier', readonly=True, 
                                 states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, 
                                 help="The analytic account related to a sales order.")
    generate_project = fields.Boolean('Generate project',default=True)
    adresse_facturation_id = fields.Many2one('res.partner', 'Invoice address')
    garantie_id = fields.Many2one('genre.garantie', 'Genre de garantie')
    description = fields.Char("Description", size=100)
    arrete = fields.Float('Arrété',default=0)
    #-------------------------------------------------------------------------------
    # COMPUTE METHODS
    #-------------------------------------------------------------------------------
            
    def _count(self):
        for order in self:
            self.env.cr.execute(
                       "select count(*) from chantier_position pos where is_title='false'and pos.order_id = %s" % order.id)
            pos_count = self.env.cr.fetchone()[0] or 0
            self.env.cr.execute(
                     '''select count(*) from project_project pro 
                       inner join sale_order so on pro.analytic_account_id = so.project_id 
                       where so.id = %s''' % order.id)
            ch_count = self.env.cr.fetchone()[0] or 0
            #self.env.cr.execute(
                    #'''select count(*) from project_task where order_id = %s''' % order.id)
            #ser_count = self.env.cr.fetchone()[0] or 0
            order.update({ 
            'pos_count':pos_count,
            'pos_count2':pos_count,
            'ch_count':ch_count,            
            #'ser_count': ser_count
                        })
            
    def _product_margin(self):        
        for sale in self:
            margin = frais = price_subtotal = net_mp = net_fab = 0
            if sale.use_positions:
                for position in sale.position_line:
                    net_mp += position.total_amount_achat * position.qty      
                    net_fab += position.total_amount_task * position.qty
                    margin += position.margin
                    frais += position.total_frais
            else:
                for line in sale.order_line:
                    if line.product_id.type == 'service':
                        net_fab += line.price_unit * line.product_uom_qty
                    else:
                        net_mp += line.price_unit * line.product_uom_qty  
                #frais = sale.total_debours
            price_subtotal = sale.amount_untaxed
            sale.update({ 
            'margin':margin,
            'margin_sale':margin,            
            'net_mp': net_mp,
            'net_fab':net_fab,
            'total_frais':frais
                        })
            if price_subtotal > 0:
                sale.update({ 
                'percentage_margin':margin / price_subtotal * 100,
                'percentage_net':net_mp / price_subtotal * 100,            
                'percentage_fab': net_fab / price_subtotal * 100,
                'percentage_frais':frais / price_subtotal * 100
                        })
    #-------------------------------------------------------------------------------
    # ORM METHODS
    #-------------------------------------------------------------------------------  
    @api.multi
    def copy(self, default=None):
        if default is None:
            default = {}
        if not default.get('project_id', False):
            default['project_id']=False
        copy = super(sale_order, self).copy(default=default)
        if copy.use_positions:   
            for line in copy.order_line:         
                line.unlink()
            self.env.cr.execute('delete from chantier_position where order_id= %s' % (copy.id))
        return copy
    #-------------------------------------------------------------------------------
    # ONCHANGE METHODS
    #-------------------------------------------------------------------------------   
    @api.multi
    @api.onchange('is_client')      
    def onchange_client_code_id(self):
        if not self.is_client:
            return
        if self.is_client == 'client':
            self.client_code = False
        else:
            self.client_code = True
        
    
            
    @api.multi
    @api.onchange('offre_id')            
    def onchange_offre_id(self):
        if self.offre_id.project_id: 
            self.project_id = self.offre_id.project_id.id
        return False
     
    #-------------------------------------------------------------------------------
    # BUTTON ACTION
    #------------------------------------------------------------------------------- 
       
    def create_position(self,pos,project):
        duration = 0.0
        is_title = False
        type='normal'
        if pos.is_title:
            is_title = True
        if self.type_offre == 'avenant':
            type= 'avenant'
        for line in pos.tache_line_ids:
            if line.product_uom.id == self.env['ir.model.data'].get_object_reference('product', 'product_uom_hour')[1]:
                duration += line.qty
            if line.product_uom.id == self.env['ir.model.data'].get_object_reference('product', 'product_uom_day')[1]:
                duration += line.qty * 8
        vals = {'name':pos.name,
              'libelle':pos.libelle,
              'ref':pos.num,
              'project_id':project.id,
              'duration':duration,
              'posit_id':pos.id,
              'is_title':is_title,
              'type_phase':type,
              'price_unit':pos.total_ht * pos.qty,
              'sequence':pos.sequence,
              'product_uom':self.env['ir.model.data'].get_object_reference('product', 'product_uom_hour')[1],
               }
        phase = self.env['project.phase'].create(vals)
        return phase
    
    def create_mp_line(self,position, line, phase, project):
        mp_obj = self.env['mission.furniture']
        vals = {'product_id':line.product_id.id,
              'product_qty':line.qty * position.qty,
              'product_uom':line.product_uom.id,
              'price_unit':line.price_unit,
              'phase_id':phase.id,
              'project_id':project.id,
              'origin':line.id,
                }
        phase_mp_line_id = mp_obj.create(vals)
        return phase_mp_line_id
 
    def create_mo_line(self,position, line, phase, project):
        mo_obj = self.env['mission.budget']
        vals = {'product_id':line.product_id.id,
              'product_uom_qty':line.qty * position.qty,
              'product_uom':line.product_uom.id,
              'price_unit':line.price_unit,
              'phase_id':phase.id,
              'project_id':project.id,
              'origin':line.id,
                }
        phase_mo_line_id = mo_obj.create(vals)
        return phase_mo_line_id
    
    @api.multi         
    def action_button_confirm(self):
            account_obj = self.env['account.analytic.account']
            project_obj = self.env['project.project']
            project_task_obj = self.env['project.task']
            project_phase_obj = self.env['project.phase']
            account = False
            for offre in self:
                frais = total_honoraire = total_honoraire2 = 0
                type = 'contract'
                if offre.project_id: 
                    account = offre.project_id
                    project = project_obj.search([('analytic_account_id', '=', account.id)],limit=1)
                if not offre.project_id and offre.generate_project and offre.use_positions:         
                        values = { 'client_id': offre.partner_id.id,
                                 'name': offre.objet or '',
                                 'manager_id':offre.user_id.id,
                                 'interlocutor_id':offre.interlocutor_id.id,
                                 'intervention_id':offre.adresse_intervention_id.id,
                                 'type':type,
                                 'to_invoice':1,
                                 'use_phases':False,
                                 'use_timesheets':1,
                                 'fix_price_invoices':1,
                                 'use_tasks':1,
                                 'charge_expenses':1,
                                 'pricelist_id':offre.pricelist_id.id,
                                 'state':'open',
                                 'company_id':offre.company_id.id         
                                    }
                        account = account_obj.create(values)         
                        self.write({'project_id':account.id})         
                        project = project_obj.search([('analytic_account_id', '=', account.id)],limit=1)
                        project.write({'partner_id':offre.partner_id.id,'intervention_id':offre.adresse_intervention_id.id,
                                       'interlocutor_id':offre.interlocutor_id.id,'imputation_project':'task', 'offre_id':offre.id,
                                       'client_project_ref':offre.client_order_ref , 'garantie_id':offre.garantie_id.id})
                                 
                if offre.generate_project:                                    
                    if len(offre.position_line) == 1 and offre.use_positions:                          
                        name = account.code or "" + " " + offre.partner_id.name or "" + " " + offre.objet or ""
                        position = offre.position_line
                        prix_vente = project.price_unit + (position.total_ht * position.qty)   
                        if offre.type_offre == 'avenant':                            
                            if project.use_phases == False :
                                phase = project_phase_obj.search([('project_id','=',project.id)])
                                if phase:
                                    for task in project.task_line:
                                        task.write({'phase_id':phase.id})                                            
                            project.write({'use_phases':True,'price_unit':prix_vente})   
                        else: 
                            project.write({'price_unit':prix_vente,'order_line_id':offre.order_line, 'position_id':position.id,
                                           'note':position.libelle, 'net_price':offre.net_mp})
                        phase = self.create_position(position, project)                                     
                        type = 'normal'                       
                        position.write({'state':'confirmed'})                                     
                        for line in position.position_line_ids:
                            self.create_mp_line(position, line, phase, project)
                        for line in position.tache_line_ids:
                            self.create_mo_line(position, line, phase, project)
                        total_honoraire = position.total_amount_product * position.qty    
                        total_honoraire2 = position.total_amount_task * position.qty                         
                        hours = offre.net_fab + account.hours_qtt_est
                        amount_max = offre.amount_total
                        frais = account.est_expenses + offre.total_frais
                        amount_max += account.amount_max 
                        fix_price_to_invoice = account.fix_price_to_invoice + offre.amount_total
                        account.write({'name':name, 'est_expenses':frais, 'hours_qtt_est':hours,'fix_price_to_invoice':fix_price_to_invoice, 
                                       'amount_max':amount_max, 'type' :type})                                                         
                        phase.write({'total_honoraire':total_honoraire, 'total_honoraire2':total_honoraire2})
                    elif len(offre.position_line) > 1 and offre.use_positions:
                        prix_vente = project.price_unit 
                        type = 'normal'
                        name = account.code or "" + " " + offre.partner_id.name or "" + " " + offre.objet or ""  
                        if offre.type_offre == 'avenant':                            
                            if project.use_phases == False :
                                phase = project_phase_obj.search([('project_id','=',project.id)],limit=1)
                                if phase:
                                    for task in project.task_line:
                                        task.write({'phase_id':phase.id})    
                        else :
                            note = offre.position_line_title[0].libelle or ""
                        for position in offre.position_line_title:
                            prix_vente += (position.total_ht * position.qty) 
                            total_honoraire = position.total_amount_product * position.qty    
                            total_honoraire2 = position.total_amount_task * position.qty 
                            phase = self.create_position(position,project)                        
                            position.write({'state':'confirmed'})
                            for line in position.position_line_ids:
                                self.create_mp_line(position, line, phase, project)
                            for line in position.tache_line_ids:
                                self.create_mo_line(position, line, phase, project)
                            phase.write({'total_honoraire':total_honoraire, 'total_honoraire2':total_honoraire2})                                         
                        hours = offre.net_fab + account.hours_qtt_est
                        amount_max = account.amount_max + offre.amount_total
                        fix_price_to_invoice = account.fix_price_to_invoice + offre.amount_total
                        frais = account.est_expenses + offre.total_frais
                        account.write({'name':name, 'est_expenses':frais, 'hours_qtt_est':hours, 
                                       'fix_price_to_invoice':fix_price_to_invoice,'amount_max':amount_max, 'type' :type}) 
                        project.write({'use_phases':True,'price_unit':prix_vente,'note':note})
                    else:
                        if offre.invoice_on_timesheets:                                             
                            for line in offre.order_line_create_task:                            
                                product_id = self.env['product.product'].search([('product_tmpl_id', '=', line.product_id.id)])                                    
                                ser_regie = account_obj.search([('code', '=', 'C001')],limit=1)
                                project_on_timesheet = project_obj.search([('analytic_account_id', '=', ser_regie.id)])
                                project_on_timesheet.write({'use_phases':False, 'type_compte_projet':'compte',
                                                            'partner_id':offre.partner_id.id}) 
                                hours = line.product_uom_qty 
                                tarif = line.product_id.list_price / line.product_uom_qty
                                cost = line.product_id.standard_price / line.product_uom_qty                               
                                if line.product_id.uom_id.id != 5:                                    
                                    hours = line.product_uom_qty * 8
                                    tarif = line.product_id.uom_id._compute_price(tarif, 5) 
                                    cost = line.product_id.uom_id._compute_price(cost, 5)
                                value_task = {'name': '%s:%s' % (line.order_id.name or '', line.name),
                                            'project_id':project_on_timesheet.id,
                                            'order_id':offre.id,
                                            'product_id':product_id[0],
                                            'user_id':line.assigned_to.id,
                                            'facturable':True,
                                            'partner_task_id':offre.partner_id.id,
                                                  }
                                task = project_task_obj.create(value_task)  
                                employee = self.env['hr.employee'].search([('user_id','=', line.assigned_to.id)],limit=1)                    
                                vals ={
                                     'user_id': line.assigned_to.id, 
                                     'name': '%s:%s' % (line.order_id.name or '', line.name), 
                                     'task_id': task.id, 
                                     'uom_id':5, 
                                     'employee_id':employee.id or False,
                                     'facturable': True,  
                                     'to_invoice': 1, 
                                     'unit_amount': hours, 
                                     'invoice_id': False,  
                                     'product_id': line.product_id.id,
                                     'account_id': ser_regie.id or False,
                                     'journal_id': employee.journal_id.id or False,}                                    
                                self.env['hr.analytic.timesheet'].create(vals) 
            return  super(sale_order, self).action_button_confirm()
 
    @api.multi
    def action_cancel(self):
        project_obj = self.env['project.project']
        invoice_obj = self.env['account.invoice'] 
        for offre in self: 
            if offre.project_id:
                analytic_id = offre.project_id.id
                project = project_obj.search([('analytic_account_id', '=', analytic_id)],limit=1)
                if project.state == 'draft':
                    invoice_ids = invoice_obj.search([('analytic_id', '=', analytic_id), ('state', 'in', ('open', 'paid'))]) 
                    if not invoice_ids:
                        self.env.cr.execute('delete from account_invoice where analytic_id= %s' % (analytic_id))
                        self.env.cr.execute('delete from project_phase where project_id= %s' % (project.id)) 
                        self.env.cr.execute('delete from project_task where project_id= %s' % (project.id))
                        project.unlink()
                        offre.project_id.unlink()                            
                    else :
                        raise odoo.osv.osv.except_osv(
                             _('Cannot cancel this sales order!'),
                             _('First cancel all invoices attached to this sales order.'))
                else :
                    raise odoo.osv.osv.except_osv(
                        _('Cannot cancel a sales order with opened project'))
            return super(sale_order, self).action_cancel()      
    #-------------------------------------------------------------------------------
    # Ces méthodes ne sont pas encore migrés
    #-------------------------------------------------------------------------------     
           
#     def _default_fiscal_year(self):
#         self.env.cr.execute('select id from account_fiscalyear where date_part(\'year\',date_stop)   =  (SELECT  date_part(\'year\', NOW()))')
#         for id in self.env.cr.fetchall():
#             return id
    

     
#     def _get_order(self, cr, uid, ids, context=None):
#         result = {}
#         for line in self.env['sale.order.line').browse(cr, uid, ids, context=context):
#             result[line.order_id.id] = True
#         return result.keys() 

#         
#     #START créer une facture finale pour les projets SER       
#     def _prepare_invoice(self, cr, uid, order, lines, context=None):
#         """Prepare the dict of values to create the new invoice for a
#            sales order. This method may be overridden to implement custom
#            invoice generation (making sure to call super() to establish
#            a clean extension chain).
#  
#            :param browse_record order: sale.order record to invoice
#            :param list(int) line: list of invoice line IDs that must be
#                                   attached to the invoice
#            :return: dict of value to create() the invoice
#         """
#         if context is None:
#             context = {}
#         journal_id = self.pool['account.invoice'].default_get(cr, uid, ['journal_id'], context=context)['journal_id']
#         if not journal_id:
#             raise osv.except_osv(_('Error!'),
#                 _('Please define sales journal for this company: "%s" (id:%d).') % (order.company_id.name, order.company_id.id))
#         #Start
#         ids_discount = []
#         for discount in order.discount_ids:
#                 if not discount.arret:
#                     ids_discount.append(discount.id) 
#         #End
#         invoice_vals = {
#           name': order.client_order_ref or '',
#           origin': order.name,
#           type': 'out_invoice',
#           reference': order.client_order_ref or order.name,
#           account_id': order.partner_invoice_id.property_account_receivable.id,
#           partner_id': order.partner_invoice_id.id,
#           journal_id': journal_id,
#           invoice_line': [(6, 0, lines)],
#           currency_id': order.pricelist_id.currency_id.id,
#           comment': order.note,
#           payment_term': order.payment_term and order.payment_term.id or False,
#           fiscal_position': order.fiscal_position.id or order.partner_invoice_id.property_account_position.id,
#           date_invoice': context.get('date_invoice', False),
#           company_id': order.company_id.id,
#           user_id': order.user_id and order.user_id.id or False,
#           section_id' : order.section_id.id,
#             #Start
#           discount_ids': [(6, 0, ids_discount)],
#           taxes':order.amount_tax_sale,
#           discount':order.discount,
#           rplp':order.rplp,
#           debours':order.total_debours,
#             #End
#         }
#  
#         # Care for deprecated _inv_get() hook - FIXME: to be removed after 6.1
#         invoice_vals.update(self._inv_get(cr, uid, order, context=context))
#         return invoice_vals      
     
     
 
    


      
