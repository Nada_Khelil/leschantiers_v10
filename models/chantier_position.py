# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import odoo.exceptions
import odoo.osv.osv
import odoo.addons.decimal_precision as dp
import logging
_logger = logging.getLogger(__name__)


     
#-------------------------------------------------------------------------------
# chantier_position
#-------------------------------------------------------------------------------
class chantier_position(models.Model):
    _name = "chantier.position" 
    _order = 'sequence' 
    
    #-------------------------------------------------------------------------------
    # FIELDS
    #-------------------------------------------------------------------------------   
    name = fields.Char('Titre', required=True)
    num = fields.Char('Numéro ')
    sequence = fields.Integer('Sequence',default=5)
    position_line_ids = fields.One2many('sale.order.line', 'posit_id', 'Raw materials') # pour position dans sale order, a comme id posit_id
    tache_line_ids = fields.One2many('sale.order.line', 'posit_tache_id', 'Tasks')
    order_id = fields.Many2one('sale.order', 'Order Reference')
    is_model = fields.Boolean('Position modèle',default=False)
    position_id = fields.Many2one('chantier.position', 'Position',domain=[('is_model', '=', True)]) # id du position modele
    qty = fields.Float('Quantity',default=1)
    prix_effectif = fields.Float('Prix Effectif', digits=dp.get_precision('Product Price'))
    libelle = fields.Html('Libellé')
    genere = fields.Boolean('generer lignes',default=False)
    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist',default=1)
    currency_id = fields.Many2one('res.currency', related='pricelist_id.currency_id', string='Currency',readonly=True)
    partner_id = fields.Many2one('res.partner', 'Customer')
    date_order = fields.Datetime('Date')
    fiscal_position = fields.Many2one('account.fiscal.position', string='Fiscal Position')
    state = fields.Selection([('draft', 'Draft Quotation'), ('confirmed', 'Confirmed')], string='Status',default='draft')
   
    total_amount_product = fields.Monetary(compute='_totals', store=True, readonly=True, string='Total raw materials')
    total_amount_task = fields.Monetary(compute='_totals', store=True, readonly=True, string='Total manpower')
    total_ht = fields.Monetary(compute='_totals', store=True, string='Total HT')
    taxes = fields.Monetary(compute='_totals', store=True, string='Taxes')
    total_unit = fields.Monetary(compute='_totals', store=True, string='Total Unitaire')
    total = fields.Monetary(compute='_totals', store=True, string='Total')
    total_ht_global = fields.Monetary(compute='_totals', store=True, string='Total Global')
    margin = fields.Float(compute='_totals', string='Margin', store=True,digits=dp.get_precision('Account'))
    total_amount_achat = fields.Float(compute='_totals', string='Part achat', store=True, digits=dp.get_precision('Account'))
    perc_amount_achat = fields.Float(compute='_totals', string='% Raw materials', store=True, digits=dp.get_precision('Account'))
    perc_amount_task = fields.Float(compute='_totals', string='% Manpower', store=True, digits=dp.get_precision('Account'))
    perc_margin = fields.Float(compute='_totals', string='% Margin', store=True, digits=dp.get_precision('Account'))
    total_frais = fields.Float(compute='_totals', string='Total frais', store=True, digits=dp.get_precision('Account'))
    perc_total_frais = fields.Float(compute='_totals', string='% Frais',store=True, digits=dp.get_precision('Account'))
    
    is_title = fields.Boolean('Use title ?',default=False)
    unit = fields.Many2one('product.uom', string='Unit')
     
   
    #-------------------------------------------------------------------------------
    # COMPUTE METHODS
    #-------------------------------------------------------------------------------
    def amount_tax_feuille(self,line):
        amount_tax = 0.0
        price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
        taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
        amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
        return amount_tax
     
    def amount_tax_task(self,line):
        amount_tax = 0.0
        price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
        taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
        amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
        return amount_tax
    
    @api.depends('tache_line_ids.price_unit', 'tache_line_ids.qty', 'position_line_ids.total_price')
    def _totals(self): 
        for rec in self: 
            tax_task = tax_pos_line=total_amount_product=total_amount_task =total_achat_product =total_ht =margin =frais =0
            for task in rec.tache_line_ids:
                total_amount_task += task.price_unit * task.qty
                tax_task += self.amount_tax_task(task)
            for feuille in rec.position_line_ids:
                total_amount_product += feuille.total_price
                total_achat_product += feuille.purchase_price
                frais += feuille.frais_livraison_speciaux + feuille.frais_usinage_total
                tax_pos_line += self.amount_tax_feuille(feuille)
                margin += feuille.purchase_price * feuille.marge / 100
            total_ht = total_amount_product + total_amount_task
            rec.total_amount_task = total_amount_task
            rec.total_amount_product = total_amount_product
            rec.total_ht= total_ht
            rec.taxes= tax_task + tax_pos_line
            rec.total_unit= tax_task + tax_pos_line + total_amount_product + total_amount_task
            rec.total_ht_global= (total_amount_product + total_amount_task) * rec.qty
            rec.total= (tax_task + tax_pos_line + total_amount_product + total_amount_task) * rec.qty
            rec.margin= margin          
            rec.total_amount_achat= total_achat_product
            rec.total_frais= frais
            if total_ht > 0:
                rec.perc_amount_achat=total_achat_product / total_ht * 100
                rec.perc_amount_task=total_amount_task / total_ht * 100
                rec.perc_margin= margin / total_ht * 100
                rec.perc_total_frais= frais / total_ht * 100
    #-------------------------------------------------------------------------------
    # ONCHANGE METHODS
    #------------------------------------------------------------------------------- 
    @api.onchange('position_id')    
    def onchange_libelle(self): 
        self.libelle = self.position_id.libelle
        self.name = self.position_id.name
        task_vals = []
        mp_line = []
        for task in self.position_id.tache_line_ids:
            tax_task_ids = []
            for tax in task.product_id.taxes_id:
                tax_task_ids.append(tax.id)
            vals = {
                'product_id':task.product_id.id,
                'tax_id':tax_task_ids,
                'name':task.product_id.name,
                'qty':task.product_uom_qty,
                'product_uom_qty':task.product_uom_qty,
                'price_unit': task.product_id.standard_price,
                'product_uom':task.product_id.uom_id.id,
                'state':'draft',
                  }
            task_vals.append(vals)
        for mp in self.position_id.position_line_ids:
            tax_ids = []
            for tax in mp.product_id.taxes_id:
                tax_ids.append(tax.id)
            vals = {
                'tax_id':tax_ids,
                'delay':mp.product_id.sale_delay or 0.0,
                'delay_fabric':mp.product_id.produce_delay or 0.0,
                'product_id':mp.product_id.id,
                'name':mp.product_id.name,
                'qty':mp.product_uom_qty,
                'product_uom_qty':mp.product_uom_qty,
                'product_uom':mp.product_id.uom_id.id,
                'state':'draft',
                   }         
            product = mp.product_id
            vals['price_revient'] = product.standard_price
            vals['type'] = product.type
            vals['type_vente'] = product.type_vente
            vals['marge'] = product.marge
            vals['frais_usinage_unitaire'] = product.frais_usinage 
            vals['frais_livraison_speciaux'] = product.frais_livraison_speciaux
            vals['longueur'] = product.longueur
            vals['largeur'] = product.largeur
            mp_line.append(vals)
        self.tache_line_ids = task_vals
        self.position_line_ids = mp_line 
        for line in self.position_line_ids:
            line.dimension_change()
            line.on_change_unit_groupe()
        
    @api.multi
    def flag_as_model(self):   
        self.is_model= True
    
    @api.multi
    def write(self, values):
        res = super(chantier_position, self).write(values)
        order_line_obj = self.env['sale.order.line']
        sale_lines = order_line_obj.search(['|', ('posit_tache_id', '=', self.id), ('posit_id', '=', self.id)])
        for line_order in sale_lines:
            line_order.write({'order_id':self.order_id.id, 'product_uom_qty':line_order.qty * self.qty})
        self.order_id._amount_all()
        return res 
      

         
    
    