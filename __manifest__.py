# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "LesChantiers",
    'version': '1.0',
    "author": "MT Consulting",
    'sequence': 5,
    "category": 'Chantiers',
    'complexity': "complexe",
    "description": """
        Gestion de chantiers
    """,
    'website': 'http://www.laboite-sarl.ch',
    'images': [],
    'init_xml': [],
    "depends": ['sale','mrp', 'project','sale_margin','purchase','fleet','purchase_requisition',
                'hr','event','crm','product_chantier_v10'
#                  'mt_action','project_timesheet','account_analytic_analysis',
#                  'project_long_term','knowledge',
#                  'analytic_contract_hr_expense',
#                  'mt_project',
#                  'remise','rplp','laboite_standard_partner',
#                  'laboite_acompte','mt_purchase','mt_sale',
#                  'mt_sale_invoicable_tasks','situation','laboite_account'
                 ],

    'data': [
              'security/security1.xml',
              'views/leschantiers.xml',
              'data/chantier_data.xml',  
              "data/donnee.sql", 
              'wizards/sale_wizard.xml',
#              "wizard/invoice_situation.xml",
#              'views/project.xml', 
              'views/donnee_de_base.xml',
              'views/position_view.xml', 
              "views/sale_view.xml",                                      
#              "views/invoice_view.xml",
#              "report/invoices_report.xml",   
#              "report/sale_layout_template.xml",
#              "report/offre.xml", 
#              "report/position.xml",
#              "report/chantier.xml",
#              "report/situation_invoice.xml",
#              "report/position_modele.xml",
#              "report/contract.xml",
#              "security/ir.model.access.csv",
#              
             
    ],
    'demo_xml': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
#  vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
